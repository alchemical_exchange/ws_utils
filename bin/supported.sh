# This file defines the set of supported applications for ws-utils install and
# update utilities. This case statement is used to set constants used in the
# scripts. 
#
# Any script using this case statement is expected to have defined
# APPLICATION_NAME, $DESKTOP_FILES, and $ICON_FILES. Also scripts that use this
# should have implemented a usage function.

case ${APPLICATION_NAME} in
  chrome)
    # Chrome shouldn't need anything as its only distrubuted as a package
    # handled by a package manager.
    ;;
  clion)
    readonly PERMISSIONS=jetbrains
    readonly DESKTOP_FILE=${DEKTOP_FILES}/jetbrains_clion.desktop
    readonly BASE_DIR=/opt/jetbrains
    readonly TARGET_DIR=${BASE_DIR}/clion
    ;;
  datagrip)
    readonly PERMISSIONS=jetbrains
    readonly DESKTOP_FILE=${DESKTOP_FILES}/jetbrains_datagrip.desktop
    readonly BASE_DIR=/opt/jetbrains
    readonly TARGET_DIR=${BASE_DIR}/datagrip
    ;;
  gitkraken)
    readonly PERMISSIONS=axosoft
    readonly DESKTOP_FILE=${DESKTOP_FILES}/axosoft_gitkraken.desktop
    readonly DESKTOP_ICON=${ICON_FILES}/gitkraken.icon
    readonly BASE_DIR=/opt/axosoft
    readonly TARGET_DIR=${BASE_DIR}/gitkraken
    ;;
  intellij)
    readonly PERMISSIONS=jetbrains
    readonly DESKTOP_FILE=${DESKTOP_FILES}/jetbrains_intellij.desktop
    readonly BASE_DIR=/opt/jetbrains
    readonly TARGET_DIR=${BASE_DIR}/intellij
    ;;
  phpstorm)
    readonly PERMISSIONS=jetbrains
    readonly DESKTOP_FILE=${DESKTOP_FILES}/jetbrains_phpstorm.desktop
    readonly BASE_DIR=/opt/jetbrains
    readonly TARGET_DIR=${BASE_DIR}/phpstorm
    ;;
  pycharm)
    readonly PERMISSIONS=jetbrains
    readonly DESKTOP_FILE=${DESKTOP_FILES}/jetbrains_pycharm.desktop
    readonly BASE_DIR=/opt/jetbrains
    readonly TARGET_DIR=${BASE_DIR}/pycharm
    ;;
  rubymine)
    readonly PERMISSIONS=jetbrains
    readonly DESKTOP_FILE=${DESKTOP_FILES}/jetbrains_rubymine.desktop
    readonly BASE_DIR=/opt/jetbrains
    readonly TARGET_DIR=${BASE_DIR}/rubymine
    ;;
  toolbox)
    readonly PERMISSIONS=jetbrains
    readonly BASE_DIR=/opt/jetbrains
    readonly TARGET_DIR=${BASE_DIR}/toolbox
    ;;
  webstorm)
    readonly PERMISSIONS=jetbrains
    readonly DESKTOP_FILE=${DESKTOP_FILES}/jetbrains_webstorm.desktop
    readonly BASE_DIR=/opt/jetbrains
    readonly TARGET_DIR=${BASE_DIR}/webstorm
    ;;
  *)
    source ${WS_UTILS_BIN}/usage.sh
    ;;
esac

