Workstation Utilities
=====================

This project is a collection of scripts and other files for easing the process
of setting up a new workstation

Included are utilites for installing various types of packaged applications.
Such as tar.gz, deb, and rpm.

Also included are some vim files and a vimrc.

To install execute ws_utils.sfx. This will extract the archive and run a script
that copies files into place and modifies the users path to add new commands.

